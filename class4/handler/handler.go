package handler

import (
	"log"
	"net/http"
	"os"
	"time"
)

func Index(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	username := r.Header.Get("username")
	w.Header().Set("username", username)
	version := os.Getenv("VERSION")
	w.Header().Set("VERSION", version)
	statusCode := 200
	w.WriteHeader(statusCode)
	msg := "Hello man, welcome to the index page."
	reqTime := time.Now().Format("2006-01-02 15:04:05")
	log.Printf("[time: %s]-host: %s-method: %s-code: %d", reqTime, r.RemoteAddr, r.Method, statusCode)
	w.Write([]byte(msg))

}

func Healthz(w http.ResponseWriter, r *http.Request) {
	statusCode := "200"
	w.Write([]byte(statusCode))
}
