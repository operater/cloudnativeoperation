package main

import (
	"os"

	"github.com/sai/class4/httpServer"
)

func main() {
	// set env
	os.Setenv("VERSION", "0.0.1")
	httpServer.HttpServer()
}
